global find_word
%include "lib.inc"
%define CELL_OFFSET 8

section .text

;Принимает два аргумента:
;Указатель на нуль-терминированную строку.
;Указатель на начало словаря.
find_word:
	xor rax, rax
	.main_loop:
		test rsi, rsi
		jz .err
		mov r9, [rsi]
		mov r10, rsi
		
		add rsi, CELL_OFFSET
		push r9
		push r10
		call string_equals
		pop r10
		pop r9
		
		cmp rax, 1
		je .end
		
		mov rsi, r9
		jmp .main_loop
	.err:
		xor rax, rax
		ret
	.end:
		mov rax, r10
		ret
