%define next_elem 0 
%macro colon 2
	%ifid %2
	
	%else
		%error "Label is incorrect"
	%endif
	%ifstr %1
	
	%else
		%error "Key is incorrect"
	%endif
	%2:
	dq next_elem
	db %1, 0
%define next_elem %2
%endmacro
