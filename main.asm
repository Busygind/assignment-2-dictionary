%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%assign MAX_KEY_SIZE 255
%define CELL_OFFSET 8

section .rodata
error: db 'Error', 0

section .bss
key: resb MAX_KEY_SIZE


section .text
global _start

_start:
  mov rsi, MAX_KEY_SIZE
  mov rdi, key
  
  call read_string
  test rax, rax
  jz .err
  
  mov rdi, rax
  mov rsi, first_word 
  call find_word
  test rax, rax
  jz .err
  add rax, CELL_OFFSET
  mov rdi, rax
  push rdi
  call string_length
  inc rax
  pop rdi
  add rdi, rax
  jmp .success
  .err:
    mov rdi, error
    call print_error
    jmp exit
  .success:
    call print_string
    jmp exit
