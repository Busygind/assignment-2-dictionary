ASM=nasm
ASM_FLAGS=-f elf64
LD=ld


main: main.o dict.o lib.o
	$(LD) -o main $^

main.o: main.asm
	$(ASM) $(ASM_FLAGS) -o main.o main.asm

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

.PHONY: clean
clean:
	rm -rf *.o
	rm main
