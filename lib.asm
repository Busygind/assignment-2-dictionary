%define NEW_LINE 0xA
%define TAB 0x9

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy
global print_error

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov		rax, 60 	; 'exit' syscall number
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .main_loop:
        cmp 	byte[rdi + rax], 0 
        je	.end
        inc 	rax
        jmp 	.main_loop
    .end:
        ret
		
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
    call 	string_length
	pop rdi
    mov 	rsi, rdi
    mov 	rdx, rax	
    mov 	rax, 1		; 'write' syscall number
    mov 	rdi, 1		; stdout descriptor
    syscall
    ret

print_error:
    xor rax, rax
    
    push rdi      	
    call string_length      
    pop rdi
    
    mov rdx, rax        	; err len -> rdx
    mov rdi, 2          	; stderr descriptor
    mov rax, 1          	; 'write' syscall number
    syscall
    ret
    
; Принимает код символа и выводит его в stdout
print_char:
    xor 	rax, rax
    push 	rdi
    mov 	rsi, rsp
    pop 	rax
    mov 	rdx, 1
    mov 	rax, 1		; 'write' syscall number
    mov 	rdi, 1		; stdout descriptor
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov 	rdi, NEW_LINE
    jmp 	print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor 	rax, rax
    mov 	rax, rdi
    mov 	r10, rsp	; save sp
    push 	0
    .loop:
		push 	rdi
        call 	div_by_ten
		pop		rdi        
		add 	rdx, '0'
        dec 	rsp
        mov 	[rsp], dl
        cmp 	rax, 0
   	jne 	.loop
    	mov 	rdi, rsp
		push	rdi
    	call 	print_string
		pop 	rdi
    	mov 	rsp, r10	; put sp back
    	ret

div_by_ten:        
	xor 	rdx, rdx
    mov 	rbx, 10
    cmp 	rax, 10
    jb .below_ten
    div 	rbx
	xor		rbx, rbx
    ret
    .below_ten:
        mov 	rdx, rax
        mov 	rax, 0
		xor		rbx, rbx
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    or 		rdi, rdi		; set flags
    jns 	.end			; if number is positive just print it!
    push 	rdi			; else add '-'
    mov 	rdi, '-'
    call 	print_char
    pop 	rdi
    neg 	rdi
    .end:
        jmp 	print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	push rdi
	push rsi
    call string_length
	pop rsi
	pop rdi
    mov r8, rax
    mov r9, rdi
    mov rdi, rsi
    call string_length
    mov r10, rax
    mov rdi, r9	
    cmp r8, r10
    jne .not_equals
    mov rax, 0
    .loop:
		mov r9b, byte [rdi + rax]
		cmp byte [rsi + rax], r9b 
		jne .not_equals
		cmp rax, r8
		je .equals
		inc rax
		jmp .loop
    .equals:
		mov rax, 1
		ret
    .not_equals:
		mov rax, 0
		ret
		
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor		rax, rax
    xor 	rdi, rdi
    push 	0
    mov 	rdx, 1			; length
    mov 	rsi, rsp		; descriptor
    syscall
    pop 	rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor 	rcx, rcx			; word length
    xor 	rax, rax
    .main_loop:
        push 	rsi
        push 	rdi
        push 	rcx
        call 	read_char 
        pop 	rcx
        pop 	rdi
        pop 	rsi

        test 	rax, rax 		; end of word
        jz 	.success

        cmp 	rax, ' ' 		
        je 	.skip_space
        cmp rax, TAB
        je 	.skip_space
        cmp 	rax, NEW_LINE
        je 	.skip_space

        mov 	[rdi+rcx], rax  ; save char
        inc 	rcx 			
        cmp 	rsi, rcx 		; check overflow
        je		.error
        jmp 	.main_loop
	.skip_space:
        cmp 	rcx, 0 			; length = flag 'word already started'
        je 	.main_loop
        jmp 	.success
    .success:
        xor 	rax, rax
        mov 	[rdi+rcx], rax 	; 0-terminator
        mov 	rdx, rcx 
        mov 	rax, rdi 
        ret
	.error:       
		xor 	rdx, rdx                                                         
        xor 	rax, rax                         
        ret
        
read_string:
    mov r8, rdi
    mov r9, rsi
    mov r10, r8
    mov rcx, 0
    cmp rsi, 0
    je .end
    .loop_until_not_space:
       push rcx
       call read_char
       pop rcx
       mov rdi, r10
       mov rsi, r9
       cmp rax, ' '
       je .loop_until_not_space
       cmp rax, '   '
       je .loop_until_not_space
    .mainloop:
       cmp rax, 0
       je .add_null_and_end
       cmp rax, NEW_LINE
       je .add_null_and_end
       mov byte [r8], al
       inc r8
       inc rcx
       cmp rcx, rsi
       je .end
       push rcx
       call read_char
       pop rcx
       mov rdi, r10
       mov rsi, r9
       jmp .mainloop
    .add_null_and_end:
       mov byte [r8], 0
       mov rax, r10
       mov rdx, rcx
       ret
    .end:
       mov rax, 0
       ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor 	rax, rax
    xor 	rcx, rcx
    mov 	rbx, 10			; divider to get digits
    .read_digit:
        xor 	r8, r8
        mov 	r8b, byte [rdi + rcx]
        sub 	r8, '0'			; set offset for ascii-code
        cmp 	r8, 0		
        jl 	.end		; no-digit char found
        cmp 	r8, 9
        jg 	.end		; no-digit char found
        mul 	rbx			
        add 	al, r8b			; save current digit
        inc 	rcx
        jmp 	.read_digit
    .parse_error:
        mov 	rdx, 0		
        ret
    .end:
        xor 	rbx, rbx		; respect convention :)
        cmp 	rcx, 0
        je 	.parse_error	; line started with not a digit char
        mov 	rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp 	byte [rdi], '-'	
    jne 	parse_uint		
    inc 	rdi				; case with neg number
    call 	parse_uint
    neg 	rax
    inc 	rdx
    ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	push 	rdi
	push	rsi
	push	rdx
    call 	string_length		; check overflow
	pop		rdx
	pop		rsi
	pop 	rdi
    cmp 	rax, rdx
    jge 	.overflow	
    xor 	rax, rax
    xor 	rdx, rdx
    .copy_next:
        mov 	dl, [rdi + rax]
        mov 	[rsi + rax], dl
        test 	dl, dl			; if string length = 0 then goto end
        je 	.end
        inc 	rax
        jmp 	.copy_next
    .overflow:
        xor 	rax, rax
    .end:
        xor 	rdx, rdx		; respect convention :)
        ret

